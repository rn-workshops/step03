/**
 * Sample React Native App with Typescript
 * https://gitlab.com/rn-workshops/step03
 *
 * @format
 */

import React, { Component } from "react";

import { StyleSheet, Text, View } from "react-native";

interface PictureCellProps {
  title: string;
}
class PictureCell extends Component<PictureCellProps> {
  render() {
    return (
      <View>
        <Text style={pictureCellStyles.title}>{this.props.title}</Text>
      </View>
    );
  }
}

const pictureCellStyles = StyleSheet.create({
  title: {
    color: "black",
  },
});

export default class App extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <PictureCell title={"Minsk"} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 26,
    flex: 1,
    backgroundColor: "#F5FCFF",
  },
});
